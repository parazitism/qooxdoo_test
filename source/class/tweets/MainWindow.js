qx.Class.define("tweets.MainWindow",
{
  extend : qx.ui.window.Window,

  construct : function()
  {
    this.base(arguments, this.tr("Tweets Main Window"), "resource/tweets/logo.png");

    // hide the window buttons
    this.setShowClose(false);
    this.setShowMinimize(false);
    this.setShowMaximize(false);

    // adjust size
    this.setWidth(250);
    this.setHeight(300);
    this.setContentPadding(0);

    // add the layout
    var layout = new qx.ui.layout.Grid(0, 0);
    this.setLayout(layout);
    layout.setRowFlex(1, 1);
    layout.setColumnFlex(0, 1);

    // toolbar
    var toolbar = new qx.ui.toolbar.ToolBar();
    this.add(toolbar, {row: 0, column: 0, colSpan: 2});

    var reloadButton = new qx.ui.toolbar.Button(this.tr("Reload"));
    toolbar.add(reloadButton);
    reloadButton.addListener("execute", function() {
      this.fireEvent("reload");
    }, this);
    reloadButton.setToolTipText(this.tr("Reload the tweets."));

    toolbar.addSpacer();

    var settingWindow = null;
    var settingsButton = new qx.ui.toolbar.Button(this.tr("Preferences"));
    toolbar.add(settingsButton);
    toolbar.setToolTipText(this.tr("Change the application settings"));
    settingsButton.addListener("execute", function () {
      if (!settingWindow) {
        settingWindow = new tweets.SettingsWindow();
        settingWindow.moveTo(320, 30);
      }
      settingWindow.open();
    }, this);

    // list
    this.__list = new qx.ui.list.List();
    this.add(this.__list, {row: 1, column: 0, colSpan: 2});

    // textarea
    this.__textarea = new qx.ui.form.TextArea();
    this.add(this.__textarea, {row: 2, column: 0});
    this.__textarea.setPlaceholder(this.tr("Enter your message here..."));
    this.__textarea.addListener("input", function(e) {
      var value = e.getData();
      postButton.setEnabled(value.length < 140 && value.length > 0);
    }, this);

    // post button
    var postButton = new qx.ui.toolbar.Button(this.tr("Post"));
    this.add(postButton, {row: 2, column: 1});
    postButton.addListener("execute", function (e) {
      this.fireDataEvent("post", this.__textarea.getValue());
    }, this);
    postButton.setToolTipText(this.tr("Post this message on the local storage"));
    postButton.setMinWidth(60);
    postButton.setEnabled(false);
  },

  members : {
    __list : null,
    __textarea : null,

    getList : function() {
      return this.__list;
    },

    clearTextarea : function () {
      this.__textarea.setValue(null);
    }
  },


  events :
  {
    "reload"  : "qx.event.type.Event",
    "post"    : "qx.event.type.Data"
  }
});