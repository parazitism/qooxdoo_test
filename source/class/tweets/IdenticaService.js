qx.Class.define("tweets.IdenticaService",
{
  extend : qx.core.Object,

  members :
  {
    __store : null,

    fetchTweets : function () {
      if (this.__store == null) {
        var url = "testService.js";
        this.__store = new qx.data.store.Jsonp();
        this.__store.setCallbackName("callback");
        this.__store.setUrl(url);
        this.__store.bind("model", this, "tweets");
      } else {
        this.__store.reload();
      }
    },

    postTweet : function (data) {
      alert(data);
      this.fireEvent("postOk");
    }
  },

  properties :
  {
    tweets : {
      nullable : true,
      event    : "changeTweets"
    }
  },

  events :
  {
    "postOk" : "qx.event.type.Event"
  }
});