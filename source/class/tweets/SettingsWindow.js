qx.Class.define("tweets.SettingsWindow",
{
  extend : qx.ui.window.Window,

  construct : function()
  {
    this.base(arguments, this.tr("Preferences"));
    this.setLayout(new qx.ui.layout.Basic());

    var form = new qx.ui.form.Form();
    var radioGroup = new qx.ui.form.RadioButtonGroup();
    form.add(radioGroup, this.tr("Language"));

    var renderer = new qx.ui.form.renderer.Single(form);
    this.add(renderer);

    var localeManager = qx.locale.Manager.getInstance();
    var locales = localeManager.getAvailableLocales();
    var currentLocale = localeManager.getLocale();
    // mark this for translation (should hold the langauge name)
    this.marktr("$$languagename");

    // create a radio button for every available locale
    for (var i = 0; i < locales.length; i++) {
      var locale = locales[i];
      var languageName = localeManager.translate("$$languagename", [], locale);
      var localeButton = new qx.ui.form.RadioButton(languageName.toString());
      // save the locale as model
      localeButton.setModel(locale);
      radioGroup.add(localeButton);

      // preselect the current locale
      if (currentLocale.slice(0,2) === locale) {
        localeButton.setValue(true);
      }
    }

    radioGroup.getModelSelection().addListener("change", function(e) {
      var newLocale = radioGroup.getModelSelection().getItem(0);
      localeManager.setLocale(newLocale);
    }, this)
  },

  members :
  {

  }
}
);